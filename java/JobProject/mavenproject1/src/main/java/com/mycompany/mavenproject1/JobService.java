/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import java.time.LocalDate;

/**
 *
 * @author sasima
 */
public class JobService {
    public static boolean checkEnableTime(LocalDate startTime, LocalDate endTime, LocalDate toDay){
       if(toDay.isBefore(startTime)){
            return false;
        }
        if(toDay.isAfter(endTime)){
            return false;
        }
        return true;
    }
}
